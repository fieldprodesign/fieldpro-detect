import React from "react"
import { graphql, Link } from "gatsby"

import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import NewHeader from "../components/sections/HeaderNew.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import Footer from "../components/sections/Footer.js"
import FooterES from "../components/sections/FooterES.js"
import FooterFR from "../components/sections/FooterFR.js"
import { H1, H2, H3, P, QuoteText } from "../components/styles/TextStyles"

import CookiesBar from "../components/sections/cookies.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

import LightCta from "../components/sections/LightCta.js"
import LightCtaFR from "../components/sections/LightCtaFR.js"
import LightCtaES from "../components/sections/LightCtaES.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"
import DarkCtaES from "../components/sections/DarkCtaES.js"
import DarkCta from "../components/sections/DarkCta.js"

class ProductTemplate extends React.Component {
  render() {
    const product = this.props.data.contentfulProductPage
    const productPages = this.props.data.allContentfulProductPage.edges

    //FETCHING BODY CONTENT IN A LIST

    const listDataOne = this.props.data.contentfulProductPage.bodySectionText1
      .json.content[0].content

    const listDataTwo = this.props.data.contentfulProductPage.bodySectionText2
      .json.content[0].content
    const listDataThree = this.props.data.contentfulProductPage.bodySectionText3
      .json.content[0].content

    var headerMenu
    if (product.language === "EN") {
      headerMenu = (
        <NewHeader
          en={`/${product.slug}`}
          fr={`/${product.slug}-fr`}
          es={`/${product.slug}-es`}
        />
      )
    }
    if (product.language === "FR") {
      var currentSlug = product.slug
      currentSlug = currentSlug.substring(0, currentSlug.length - 3)
      headerMenu = (
        <NewHeaderFR
          en={`/${currentSlug}`}
          fr={`/${currentSlug}-fr`}
          es={`/${currentSlug}-es`}
        />
      )
    }
    if (product.language === "ES") {
      var currentSlugEs = product.slug
      currentSlugEs = currentSlugEs.substring(0, currentSlugEs.length - 3)
      headerMenu = (
        <NewHeaderES
          en={`/${currentSlugEs}`}
          fr={`/${currentSlugEs}-fr`}
          es={`/${currentSlugEs}-es`}
        />
      )
    }

    var footerMenu
    if (product.language === "EN") {
      footerMenu = <Footer />
    }
    if (product.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (product.language === "ES") {
      footerMenu = <FooterES />
    }
    //Modal
    var ctaModal
    if (product.language === "EN") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCta />
        </CTAClassWrapper>
      )
    }
    if (product.language === "FR") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaFR />
        </CTAClassWrapper>
      )
    }
    if (product.language === "ES") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaES />
        </CTAClassWrapper>
      )
    }

    const isBrowser = typeof window !== "undefined"
    //Hero image
    var heroimage

    if (
      product.slug === "retail-execution" ||
      product.slug === "retail-execution-fr" ||
      product.slug === "retail-execution-es" ||
      product.slug === "monitoring" ||
      product.slug === "monitoring-fr" ||
      product.slug === "monitoring-es" ||
      product.slug === "mobile-inspection" ||
      product.slug === "mobile-inspection-fr" ||
      product.slug === "mobile-inspection-es" ||
      product.slug === "field-force-tracking" ||
      product.slug === "field-force-tracking-fr" ||
      product.slug === "field-force-tracking-es"
    ) {
      if (isBrowser) {
        if (window.innerWidth < 451) {
          heroimage = (
            <HeroImage
              style={{
                width: "400px",

                position: "absolute",
                right: "-20px",
                top: "1000px",
              }}
            >
              <img src={product.bannerImage.file.url} alt="" />
            </HeroImage>
          )
        } else {
          heroimage = (
            <HeroImage
              style={{
                width: "1200px",

                position: "absolute",
                right: "-580px",
                top: "160px",
              }}
            >
              <img src={product.bannerImage.file.url} alt="" />
            </HeroImage>
          )
        }
      }
    } else {
      heroimage = (
        <HeroImage
          style={{
            margin: "160px 0 0 -80px",
          }}
        >
          <img src={product.bannerImage.file.url} alt="" />
        </HeroImage>
      )
    }
    //BottomModal
    var bottomCtaModal
    if (product.language === "EN") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCta />
        </BottomCTAWrapper>
      )
    }
    if (product.language === "FR") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaFR />
        </BottomCTAWrapper>
      )
    }
    if (product.language === "ES") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaES />
        </BottomCTAWrapper>
      )
    }

    //Background image for testimonials section
    var background = product.testimonialBackgroundImage.file.url
    return (
      <Layout>
        <SEO title={product.pageName} description={product.pageDescription} />
        {headerMenu}
        <HeroWrapper>
          <HeroSection>
            <Content>
              <Title>{product.bannerTitle}</Title>

              <Description>{product.bannerText}</Description>
              <CallToAction>{ctaModal}</CallToAction>
            </Content>
            {heroimage}
          </HeroSection>
        </HeroWrapper>
        <BodyWrapper>
          <BodyGroup>
            <BodyContentImage>
              <img src={product.bodySectionImage1.fluid.src} alt="" />
            </BodyContentImage>
            <BodyContentSection>
              <ContentTitle>{product.bodySectionTitle1}</ContentTitle>
              <ContentRow>
                <ListGroup>
                  {listDataOne.map(ListItemOne => {
                    return (
                      <ListItem>
                        {ListItemOne.content[0].content[0].value}
                      </ListItem>
                    )
                  })}
                </ListGroup>
              </ContentRow>
              <TextCta>
                <a
                  style={{ color: "#febd55" }}
                  href={
                    product.bodySectionLink1.json.content[0].content[1].data.uri
                  }
                >
                  {
                    product.bodySectionLink1.json.content[0].content[1]
                      .content[0].value
                  }
                </a>
                <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
              </TextCta>
            </BodyContentSection>
          </BodyGroup>
          <BodyGroupTwo>
            <BodyContentSection>
              <ContentTitle>{product.bodySectionTitle2}</ContentTitle>
              <ContentRow>
                <ListGroup>
                  {listDataTwo.map(ListItemTwo => {
                    return (
                      <ListItem>
                        {ListItemTwo.content[0].content[0].value}
                      </ListItem>
                    )
                  })}
                </ListGroup>
              </ContentRow>

              <TextCta>
                <a
                  style={{ color: "#febd55" }}
                  href={
                    product.bodySectionLink2.json.content[0].content[1].data.uri
                  }
                >
                  {
                    product.bodySectionLink2.json.content[0].content[1]
                      .content[0].value
                  }
                </a>
                <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
              </TextCta>
            </BodyContentSection>
            <BodyContentImageTwo>
              <img src={product.bodySectionImage2.file.url} alt="" />
            </BodyContentImageTwo>
          </BodyGroupTwo>
          <BodyGroup>
            <BodyContentImage>
              <img src={product.bodySectionImage3.fluid.src} alt="" />
            </BodyContentImage>
            <BodyContentSection>
              <ContentTitle>{product.bodySectionTitle3}</ContentTitle>
              <ContentRow>
                <ListGroup>
                  {listDataThree.map(ListItemThree => {
                    return (
                      <ListItem>
                        {ListItemThree.content[0].content[0].value}
                      </ListItem>
                    )
                  })}
                </ListGroup>
              </ContentRow>
              <TextCta>
                <a
                  style={{ color: "#febd55" }}
                  href={
                    product.bodySectionLink3.json.content[0].content[1].data.uri
                  }
                >
                  {
                    product.bodySectionLink3.json.content[0].content[1]
                      .content[0].value
                  }
                </a>
                <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
              </TextCta>
            </BodyContentSection>
          </BodyGroup>
        </BodyWrapper>
        <TestimonialSection style={{ backgroundImage: `url(${background})` }}>
          <TestimonialCardWrapper>
            <TestimonialCardBox>
              <TextBlock>
                <Quotation>"</Quotation>
                <Quote>{product.quote.quote}"</Quote>
              </TextBlock>
              <Citation>
                <PersonName>{product.citationName} </PersonName>
                <Position>{product.citationPosition}</Position>

                <CompanyName>{product.citationCompany}</CompanyName>
              </Citation>
            </TestimonialCardBox>
          </TestimonialCardWrapper>
        </TestimonialSection>
        <BottomWrapper>
          <SectionCardTitle>{product.linkCardSectionTitle}</SectionCardTitle>
          <SectionCards>
            <LinkCardWrapper>
              {productPages.map(parents => {
                if (product.language === parents.node.language) {
                  if (product.slug !== parents.node.slug) {
                    return (
                      <Link
                        to={
                          parents.node.linkCardUrl.json.content[0].content[1]
                            .data.uri
                        }
                      >
                        <CardBox>
                          <LinkCardIcon>
                            <img
                              src={parents.node.linkCardIcon.fluid.src}
                              alt=""
                            />
                          </LinkCardIcon>
                          <LinkCardTitle>
                            {parents.node.linkCardTitle}
                          </LinkCardTitle>
                          <LinkCardText>
                            {parents.node.linkCardText}
                          </LinkCardText>
                        </CardBox>
                      </Link>
                    )
                  }
                }
                return null
              })}
            </LinkCardWrapper>
          </SectionCards>
          <BottomCallToAction>
            <CtaBackground>
              <img
                style={{ width: "72em" }}
                src={product.ctaBgIllustration.fluid.src}
                alt="Illustration"
              />
            </CtaBackground>
            <CtaContent>
              <CtaText>
                <Tagline style={{ color: "#fefefe" }}>
                  {product.callToAction}
                </Tagline>
                <CtaGroup>{bottomCtaModal}</CtaGroup>
              </CtaText>
            </CtaContent>
          </BottomCallToAction>
        </BottomWrapper>
        {footerMenu}
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default ProductTemplate

export const pageQuery = graphql`
  query ProductPageBySlug($slug: String!) {
    contentfulProductPage(slug: { eq: $slug }) {
      language
      pageName
      pageDescription
      bannerTitle
      bannerText
      bannerImage {
        file {
          url
        }
      }

      bodySectionImage1 {
        fluid {
          src
        }
      }
      bodySectionTitle1
      bodySectionText1 {
        json
      }
      bodySectionLink1 {
        json
      }
      bodySectionImage2 {
        file {
          url
        }
      }
      bodySectionTitle2
      bodySectionText2 {
        json
      }
      bodySectionLink2 {
        json
      }
      bodySectionImage3 {
        fluid {
          src
        }
      }
      bodySectionTitle3
      bodySectionText3 {
        json
      }
      bodySectionLink3 {
        json
      }
      testimonialBackgroundImage {
        file {
          url
        }
      }
      citationName
      citationPosition
      citationCompany
      quote {
        quote
      }
      linkCardSectionTitle
      callToAction
      ctaBgIllustration {
        fluid {
          src
        }
      }
      slug
    }
    allContentfulProductPage {
      edges {
        node {
          linkCardIcon {
            fluid {
              src
            }
          }
          linkCardTitle
          linkCardText
          slug
          language
          linkCardUrl {
            json
          }
        }
      }
    }
  }
`

const HeroWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
const BodyWrapper = styled.div`
  max-width: 1280px;
  margin: 160px auto;
`
const BottomWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
const HeroSection = styled.div`
  height: 900px;
  position: relative;

  display: grid;
  grid-template-columns: repeat(2, auto);
  @media (max-width: 830px) {
    height: auto;
    grid-template-columns: repeat(1, auto);
    margin: 0 0 480px 0;
    padding: 0;
  }
  @media (max-width: 450px) {
    height: auto;
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
const Content = styled.div`
  max-width: 640px;
  margin: 280px 100px 0 0;
  @media (max-width: 830px) {
    max-width: 720px;
    padding: 0px 120px 0px 120px;
    margin: 240px 0 80px 0px;
  }
  @media (max-width: 450px) {
    max-width: 350px;
    padding: 0px 40px 0px 40px;
    margin: 120px 0 80px 0px;
  }
`
const HeroImage = styled.div`
  @media (max-width: 830px) {
    img {
      position: absolute;

      right: 0;
      left: -160px;
      top: 520px;
      max-width: 640px;
      margin: 40px 0px 80px 0px;
    }
  }
  @media (max-width: 450px) {
    img {
      position: absolute;

      right: 0;
      left: -10px;
      top: 20;
      max-width: 350px;
      margin: -200px 0px 80px 0px;
    }
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;
`
const Description = styled(P)`
  max-width: 880px;
`
const CallToAction = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    margin: 20px 0 0 0;
    flex-direction: column;
    grid-gap: 20px;
  }
`

const BodyGroup = styled.div`
  margin: 0px 0 120px 0;
  display: grid;

  grid-template-columns: 480px auto;
  gap: 160px;
  @media (max-width: 830px) {
    grid-template-columns: auto;
    gap: 20px;
    margin: 0px 20px 0 20px;
    padding: 0 120px 0 120px;
  }
  @media (max-width: 450px) {
    grid-template-columns: auto;
    gap: 20px;
    margin: 0px 20px 0 20px;
    padding: 0;
  }
`
const BodyGroupTwo = styled.div`
  margin: 120px 0 120px 0;
  display: grid;

  grid-template-columns: 480px auto;
  gap: 240px;
  @media (max-width: 830px) {
    margin: 0 20px 0 20px;
    padding: 0 120px 0 120px;
    grid-template-columns: auto;
    display: flex;
    flex-direction: column-reverse;
    gap: 30px;
  }
  @media (max-width: 450px) {
    margin: 0 20px 0 20px;

    grid-template-columns: auto;
    display: flex;
    flex-direction: column-reverse;
    gap: 30px;
  }
`
const BodyContentSection = styled.div`
  max-width: 480px;
  @media (max-width: 830px) {
    max-width: 480px;
    margin: 0;
    padding: 0;
  }
  @media (max-width: 450px) {
    max-width: 320px;
    margin: 0;
    padding: 0;
  }
`
const BodyContentImage = styled.div`
  width: 800px;
  margin: 0 0 0 -160px;
  @media (max-width: 830px) {
    width: auto;
    margin: 40px 0 -40px 0px;
  }
  @media (max-width: 450px) {
    width: auto;
    margin: 40px 0 -40px 0px;
  }
`
const BodyContentImageTwo = styled.div`
  width: 600px;

  @media (max-width: 830px) {
    margin: 80px 0 0 0px;
    padding: 0;

    width: auto;
  }
  @media (max-width: 450px) {
    margin: 80px 0 0 0px;
    padding: 0;

    width: auto;
  }
`
const ContentTitle = styled(H3)``
const ContentRow = styled(P)``

const TextCta = styled.p`
  padding-top: 20px;
  font-weight: 600;
  font-size: 1.25em;
  color: #febd55;
  cursor: pointer;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`
const ArrowButton = styled.img`
  padding-left: 8px;
`
const TestimonialSection = styled.div`
  max-width: 1280px;
  margin: 240px auto;

  img {
    height: 600px;
    width: 1280px;
  }
  @media (max-width: 830px) {
    width: auto;
    margin: 120px 0 120px 0;
    padding: 40px 0px 40px 0px;
  }
  @media (max-width: 450px) {
    max-width: 400px;
    margin: 120px 0 120px 0;
    padding: 40px 0px 40px 0px;
  }
`

const TestimonialCardWrapper = styled.div`
  padding: 72px 0px 72px 80px;
  @media (max-width: 830px) {
    max-width: 520px;
    padding: 0px 0px 0px 0px;
  }
  @media (max-width: 450px) {
    max-width: 360px;
    padding: 0px 0px 0px 0px;
  }
`
const TestimonialCardBox = styled.div`
  max-width: 600px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;
  @media (max-width: 830px) {
    max-width: 520px;

    padding: 20px 20px;
  }
  @media (max-width: 450px) {
    max-width: 360px;

    padding: 20px 20px;
  }
`
const TextBlock = styled.div`
  padding: 40px 40px 0px 40px;
  display: grid;
  grid-template-columns: auto auto;
  @media (max-width: 830px) {
    padding: 20px 20px 0 20px;
  }
  @media (max-width: 450px) {
    padding: 10px 10px 0 10px;
  }
`
const Quotation = styled(QuoteText)`
  position: absolute;
  left: 500;
  margin-left: -12px;
`
const Quote = styled(QuoteText)``
const Citation = styled.div`
  padding: 0px 40px 0 40px;
  display: grid;
  grid-template-rows: 20px 20px auto;
  grid-gap: 10px;
  @media (max-width: 450px) {
    gap: 20px;

    padding: 10px 10px 0 10px;
  }
`
const PersonName = styled.p`
  font-size: 20px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: normal;
  color: #2c2c2c;
`

const Position = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
  @media (max-width: 450px) {
    line-height: 1;
  }
`
const CompanyName = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
`
const SectionCardTitle = styled(H2)``
//LINK CARDS STYLING
const SectionCards = styled.div`
  margin: -60px 0 240px 0;
  @media (max-width: 450px) {
    margin: 0px 0 120px 0;
  }
`
const LinkCardWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 40px;
  padding: 80px 0px 0px 0px;
  @media (max-width: 830px) {
    padding: 80px 0 0 20px;
    display: grid;
    grid-template-columns: repeat(2, auto);
    grid-gap: 40px;
  }
  @media (max-width: 450px) {
    max-width: 300px;
    padding: 0 0 0 20px;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`

const CardBox = styled.div`
  width: 400px;
  height: 360px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;
  :hover {
    background: #124e5d;
    color: #fefefe;
  }
  div {
    padding: 20px 0px 0px 0px;
    margin: 0;
  }
  :hover div {
    filter: invert();
  }
  @media (max-width: 830px) {
    width: auto;
    padding: 0;
  }
`
const LinkCardIcon = styled.div`
  padding: 20px 0px 0px 0px;
  margin: 0;
`

const LinkCardTitle = styled(H3)`
  margin: -50px 24px 0px 32px;
`
const LinkCardText = styled(P)`
  padding: 20px 24px 0px 32px;
`
//Bottom cta
//Bottom Call To Action
const BottomCallToAction = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  margin: 200px 0px 280px 0px;
  padding: 0;
  @media (max-width: 830px) {
    width: auto;
  }
  @media (max-width: 450px) {
    width: 20em !important;
    margin: 100px 0px;
  }
`
const CtaBackground = styled.div`
  position: absolute;
  z-index: 1;
  align-self: center;
`

const CtaContent = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  background: #124e5d;
  box-shadow: 0 0.5em 2em #2c2c2c18;
  margin: 0 0 4em 0;
  padding: 2.5em 8em 7.5em 8em;
  @media (max-width: 32em) {
    width: 24em;
    margin: 0 0 4em 0;
    padding: 1em 2em 3em 2em;
  }
  @media (max-width: 450px) {
    width: 21em;
  }
`
const Tagline = styled(H2)`
  padding: 10px 0px 0px 0px;
  max-width: 900px;
  @media (max-width: 450px) {
    padding: 10px 0px 40px 0px;
    width: auto;
  }
`
const CtaText = styled.div`
  position: relative;
  align-self: center;

  color: #2a2a2a;
  max-width: 56em;

  z-index: 2;
  h2 {
    font-size: 3.25em;
  }
  @media (max-width: 32em) {
    width: 24em;
    margin: 0;
    padding: 0;
    h2 {
      font-size: 2.5em;
      margin: 1.2em 1.2em 1em 0;
    }
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 0;
    padding: 0;
  }
`

const CtaGroup = styled.div`
  margin: -20px 0 0 0;
  max-width: 640px;
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`

const ListGroup = styled.ul``
const ListItem = styled.li`
  list-style-image: url("/images/green-check-mark.svg");
`

////CTA AT TOP
const CTAClassWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
////CTA AT BOTTOM
const BottomCTAWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
