import React, { useState, useEffect } from "react"
import styled from "styled-components"

import { H1, BodyMain, Caption } from "../style/TextStyles.js"

function Hero(props) {
  const [offsetY, setOffsetY] = useState(0)
  const handleScroll = () => setOffsetY(window.pageYOffset)

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)

    return () => window.removeEventListener("scroll", handleScroll)
  }, [])
  return (
        <HeroSectionBackground>
          <HeroIllustration
            style={{ transform: `translateY(-${offsetY * 0.3}px)` }}
          >
            <img src={require("../images/hero_text_img.png")} alt="" />
          </HeroIllustration>
          <HeroSectionWrapper>
            <HeroContentWrapper>
              <HeroText>
                <MainHeading>
                  Optimize field sales with image recognition
                </MainHeading>
                <Description>Leverage our  Automated Image Recognition to analyze position, price, share, competitors, out-of-stocks, and more.
                </Description>
              </HeroText>
            </HeroContentWrapper>
          </HeroSectionWrapper>
        </HeroSectionBackground>
      )
    }

export default Hero
export const HeroSectionBackground = styled.div`
  padding: 0 !important;
  margin: 0px !important;
  height: 880px;
  width: 100vw;
  position: relative;

  left: -8px;
  background-color: #fff;

  @media (max-width: 830px) {
    width: 100vw;
    margin: 0 0 0 0px;
    height: 1180px;
    background-color: #fff;
  }
  @media (max-width: 512px) {
    width: 100vw;
    margin: 0 0 0 0px;

    height: 1000px;
    background-color: #fff;
  }
`
export const HeroSectionWrapper = styled.div`
  position: relative;
  max-width: 1280px;
  margin: 0 auto;
`
export const HeroContentWrapper = styled.div`
  padding: 400px auto;
  display: grid;
  grid-template-columns: repeat(2, auto);
`
export const HeroText = styled.div`
  padding: 300px 0px 0px 0px;
  max-width: 620px;

  @media (max-width: 830px) {
    padding: 200px 40px 80px 40px;
    margin: 0 0 0 0px;
    max-width: 672px;
  }
  @media (max-width: 512px) {
    padding: 200px 24px 80px 24px;
    margin: 0 0 0 0px;
    max-width: 350px;
  }
`
export const MainHeading = styled(H1)`
  color: #05242E;
  padding: 0 0 16px 0;
`
export const Description = styled(BodyMain)`
  color: #05242E;
  padding: 0 0 10px 0;
`
export const TopCta = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
export const HeroIllustration = styled.div`
  width: 50%;
  position: absolute;

  top: 280px;
  left: 60px;
  @media (max-width: 830px) {
    display: none;
  }
`
